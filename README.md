# chatgpt聊天前端源代码
[访问页面](http://hoppinzq.com/chat/index.html)
#### 介绍
模仿chatgpt写的前端页面，我就贴几张截图就行了，剩下的你们自己玩或者改代码就行了。前端所有的元数据都在webMetaData里
![首页](ChromeCore_vYE5ZuMIrE.png)
![夜间模式](ChromeCore_gioSMs7hQ0.png)
![渲染代码，表格等](ChromeCore_zCIH0K87FB.png)
![元数据管理](ChromeCore_i6ly5oah2n.png)
![自适应](ChromeCore_mWKBCwlUOI.png)
![图片生成](image.png)
![代码补全](chrome_W2tbs7yjV2.png)
### 后端服务
[在这里](https://gitee.com/hoppin/java-service-management-system/tree/master/hoppinzq-chatgpt)，后端我懒得拆出来了，就放在原来的管理系统作为一个模块。


